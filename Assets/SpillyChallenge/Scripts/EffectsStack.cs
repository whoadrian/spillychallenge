﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

// Chains an arbitrary number of effects onto an image

public class EffectsStack : MonoBehaviour
{
	// When should this stack render its effects?
	public enum UpdateMode { Update, LateUpdate }
	public UpdateMode updateMode = UpdateMode.Update;
	
	// Is this effect stack playing?
	public bool play = true;

	[Header("Rendering")]

	// Base texture
	public RenderTexture inputTexture;
	[Space]
	// Chain of effects
	public List<Effect> effectStack;
	[Space]
	// Output texture
	public RenderTexture outputTexture;

	[Header("Post Process")]
	public bool sendToPostProcess = false;
	public PostProcessStack postProcessStack;
	public Material overridePostProcessMaterial;

	// Helper flag for ping-pong'ing textures
	private bool _isPing = true;



	private void Start()
	{
		// Send to post process stack
		if (sendToPostProcess && postProcessStack)
		{
			postProcessStack.AddEffect(this);
		}
	}

	private void OnEnable()
	{
		//
	}

	private void OnDisable()
	{
		// Clear output texture
		if (outputTexture)
		{
			RenderTexture rt = UnityEngine.RenderTexture.active;
			UnityEngine.RenderTexture.active = outputTexture;
			GL.Clear(true, true, Color.clear);
			UnityEngine.RenderTexture.active = rt;
		}
	}

	public void Update()
	{
		if (updateMode == UpdateMode.Update)
			Render();
	} 

	public void LateUpdate()
	{
		if (updateMode == UpdateMode.LateUpdate)
			Render();
	}

	private void Render()
	{
		if (inputTexture == null || outputTexture == null)
			return;

		if (!play)
			return;

		// Prepare temporary ping-pong textures
		RenderTexture ping = RenderTexture.GetTemporary(inputTexture.descriptor);
		RenderTexture pong = RenderTexture.GetTemporary(inputTexture.descriptor);

		// Feed the base texture
		_isPing = true;
		Graphics.Blit(inputTexture, ping);

		// Prepare chain of effects
		foreach (Effect e in effectStack)
			e.ApplyMaterialValues();

		// Apply chain of effects
		foreach (Effect e in effectStack)
		{
			Graphics.Blit((_isPing) ? ping : pong, (_isPing) ? pong : ping, e.material);

			// Switch ping-pong state
			_isPing = !_isPing;
		}
		
		// Write into the output texture
		Graphics.Blit((_isPing) ? ping : pong, outputTexture);

		// Release memory
		RenderTexture.ReleaseTemporary(ping);
		RenderTexture.ReleaseTemporary(pong);
	} 
}

#if UNITY_EDITOR

// Do here reorderable lists

#endif