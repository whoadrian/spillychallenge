﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Effect // Effect data class
{
	public Material material;               // Material used for rendering
	public List<ShaderTexture> textures;    // Textures that should be assigned to this material
	public List<ShaderInt> ints;            // Ints for this material
	public List<ShaderFloat> floats;        // Floats for this material
	public List<ShaderColor> colors;        // Colors for this material

	// Apply stored data to the material of this class
	public void ApplyMaterialValues()
	{
		if (material == null)
		{
			Debug.LogError("Effect material is null!");
		}

		// Textures
		foreach (ShaderTexture t in textures)
			material.SetTexture(t.name, t.tex);

		// Ints
		foreach (ShaderInt i in ints)
			material.SetInt(i.name, i.value);

		// Floats
		foreach (ShaderFloat f in floats)
			material.SetFloat(f.name, f.value);

		// Colors
		foreach (ShaderColor c in colors)
			material.SetColor(c.name, c.value);
	}

	// Deep copy this class into another one
	public void CopyInto(ref Effect other)
	{
		other.material = material;
		other.textures = new List<ShaderTexture>();

		foreach (ShaderTexture t in textures)
			other.textures.Add(new ShaderTexture(t.name, t.tex));
		foreach (ShaderInt i in ints)
			other.ints.Add(new ShaderInt(i.name, i.value));
		foreach (ShaderFloat f in floats)
			other.floats.Add(new ShaderFloat(f.name, f.value));
		foreach (ShaderColor c in colors)
			other.colors.Add(new ShaderColor(c.name, c.value));
	}
}

[System.Serializable]
public class ShaderFloat
{
	public string name;
	public float value;

	public ShaderFloat(string name, float value)
	{
		this.name = name;
		this.value = value;
	}
}

[System.Serializable]
public class ShaderColor
{
	public string name;
	public Color value;

	public ShaderColor(string name, Color value)
	{
		this.name = name;
		this.value = value;
	}
}

[System.Serializable]
public class ShaderInt
{
	public string name;
	public int value;

	public ShaderInt(string name, int value)
	{
		this.name = name;
		this.value = value;
	}
}

[System.Serializable]
public class ShaderTexture
{
	public string name;
	public Texture tex;

	public ShaderTexture(string name, Texture tex)
	{
		this.name = name;
		this.tex = tex;
	}
}