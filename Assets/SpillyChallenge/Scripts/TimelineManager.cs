﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Video;

// Manages the timeline. Makes sure everything is synced

public class TimelineManager : MonoBehaviour
{
	// Video players
	public VideoController videoController;
	// Should the video playback start when videos are prepared?
	public bool autoplayVideosWhenReady = true; 

	// Timeline director
	private PlayableDirector _director;
	
	// Timeline state behaviour
	private enum State { None, Play, Pause }
	private State _state = State.None;
	private State TimelineState
	{
		get { return _state; }
		set
		{
			if (_state == value)
				return;

			switch (value)
			{
				case State.Pause:
					_director.Pause();
					break;

				case State.Play:
					_director.Play();
					break;
			}
			_state = value;

			Debug.Log("Timeline State : " + _state.ToString());
		}
	}

	private void Start ()
	{
		_director = GetComponent<PlayableDirector>();

		if (videoController)
			videoController.Prepare(VideosReady);
	}

	private void VideosReady()
	{
		TimelineState = State.Play;

		if (autoplayVideosWhenReady)
			if (videoController)
				videoController.Play();
	}

	#region UI_Events

	public void OnPlay()
	{
		TimelineState = State.Play;

		if (autoplayVideosWhenReady)
			if (videoController)
				videoController.Play();
	} 

	public void OnPause()
	{
		TimelineState = State.Pause;

		if (autoplayVideosWhenReady)
			if (videoController)
				videoController.Pause();
	}

	#endregion
}
