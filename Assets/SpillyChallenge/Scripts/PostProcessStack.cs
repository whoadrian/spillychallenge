﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Camera script. Effects Stacks add themselves via the AddEffect(e) method to be rendered and blended into a final image, on screen, via a post-process stack.

public class PostProcessStack : MonoBehaviour
{
	// Default blending material
	public string blitTexName;
	public Material blitMaterial;

	// List of effects stacks, from first to last to be rendered.
	private List<EffectsStack> _effects = new List<EffectsStack>();


	// How should the stacks be ordered for rendering
	public class SiblingIndexComparer : IComparer<EffectsStack>
	{
		public int Compare(EffectsStack x, EffectsStack y)
		{
			// Order by sibling index (assuming all effects are under the same parent)
			int xIndex = x.transform.GetSiblingIndex();
			int yIndex = y.transform.GetSiblingIndex();

			return (xIndex == yIndex) ? 0 : ((xIndex > yIndex) ? 1 : -1);
		}
	}

	// Subscribe effect to the post-process chain
	public void AddEffect(EffectsStack effect)
	{
		if (_effects.Contains(effect))
			return;

		// Add and sort
		_effects.Add(effect);
		_effects.Sort(new SiblingIndexComparer());
	}

	// Render
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		// Do nothing if no effects
		if (_effects.Count == 0)
		{
			Graphics.Blit(source, destination);
			return;
		}
		
		// Ping-pong between the effects until all have been processed
		RenderTexture ping = RenderTexture.GetTemporary(source.descriptor);
		RenderTexture pong = RenderTexture.GetTemporary(source.descriptor);

		bool isPong = false;
		bool init = false;
		for (int i = 0; i<_effects.Count; i++)
		{
			if (!_effects[i].enabled)
				continue;

			// First effect gets straight into the ping-pong sequence
			if (!init)
			{
				Graphics.Blit(_effects[i].outputTexture, ping);
				init = true;
				continue;
			}

			// Check if effect wants the blending material to be overriden
			Material mat = (_effects[i].overridePostProcessMaterial != null) ? _effects[i].overridePostProcessMaterial : blitMaterial;

			// Prepare material
			mat.SetTexture(blitTexName, _effects[i].outputTexture);
			// Blend
			Graphics.Blit((isPong) ? pong : ping, (isPong) ? ping : pong, mat);
			
			// Switch ping-pong state
			isPong = !isPong;
		}

		// Render to screen
		Graphics.Blit((isPong) ? pong : ping, destination);

		// Release memory
		RenderTexture.ReleaseTemporary(ping);
		RenderTexture.ReleaseTemporary(pong);
	}
}
