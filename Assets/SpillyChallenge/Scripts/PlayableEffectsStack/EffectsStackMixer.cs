﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

// Timeline mixer for the Effects Stack script. Handles all relevant timeline clips and blend in between them.

public class EffectsStackMixer : PlayableBehaviour
{
	// Relevant default data for the Effects Stack materials list
	ShaderInt _defaultInt = new ShaderInt("", 0);
	ShaderFloat _defaultFloat = new ShaderFloat("", 0);
	ShaderColor _defaultColor = new ShaderColor("", Color.clear);

	EffectsStack trackBinding; // Scene reference
	bool firstFrameHappened; // Has the first frame been processed already?


	// Process each frame in this track
	public override void ProcessFrame(Playable playable, FrameData info, object playerData)
	{
		// Get scene reference 
		trackBinding = playerData as EffectsStack;

		if (trackBinding == null)
			return;

		// Initialise on first frame
		if (!firstFrameHappened)
		{
			// Find default values
			for (int i=0; i<trackBinding.effectStack.Count; i++)
			{
				// Int
				for (int intIndex = 0; intIndex<trackBinding.effectStack[i].ints.Count; intIndex++)
					if (trackBinding.effectStack[i].ints[intIndex].name == _defaultInt.name)
						_defaultInt.value = trackBinding.effectStack[i].ints[intIndex].value;

				// Float
				for (int floatIndex = 0; floatIndex < trackBinding.effectStack[i].floats.Count; floatIndex++)
					if (trackBinding.effectStack[i].floats[floatIndex].name == _defaultFloat.name)
						_defaultFloat.value = trackBinding.effectStack[i].floats[floatIndex].value;

				// Color
				for (int colorIndex = 0; colorIndex < trackBinding.effectStack[i].colors.Count; colorIndex++)
					if (trackBinding.effectStack[i].colors[colorIndex].name == _defaultColor.name)
						_defaultColor.value = trackBinding.effectStack[i].colors[colorIndex].value;
			}
			firstFrameHappened = true;
		}
		
		// Playables count
		int inputCount = playable.GetInputCount();

		// Temporary relevant data, used for blending
		ShaderInt blendedInt = new ShaderInt("", 0);
		ShaderFloat blendedFloat = new ShaderFloat("", 0);
		ShaderColor blendedColor = new ShaderColor("", Color.clear);

		// Weight info
		float totalWeight = 0f;
		float greatestWeight = 0f;
		int currentInputs = 0;

		// Go through all playables
		for (int i = 0; i < inputCount; i++)
		{
			// Get playable data
			float inputWeight = playable.GetInputWeight(i);
			ScriptPlayable<EffectsStackBehaviour> inputPlayable = (ScriptPlayable<EffectsStackBehaviour>)playable.GetInput(i);
			EffectsStackBehaviour input = inputPlayable.GetBehaviour();

			// Get blended int (if any)
			if (!string.IsNullOrEmpty(input.intData.name))
			{
				blendedInt.name = input.intData.name;
				blendedInt.value = (int)(blendedInt.value + input.intData.value * inputWeight);
			}

			// Get blended float (if any)
			if (!string.IsNullOrEmpty(input.floatData.name))
			{
				blendedFloat.name = input.floatData.name;
				blendedFloat.value += input.floatData.value * inputWeight;
			}

			// Get blended color (if any)
			if (!string.IsNullOrEmpty(input.colorData.name))
			{
				blendedColor.name = input.colorData.name;
				blendedColor.value += input.colorData.value * inputWeight;
			}

			// Gather weights
			totalWeight += inputWeight;

			// Record greatest weight
			if (inputWeight > greatestWeight)
			{
				greatestWeight = inputWeight;
			}

			// If there's no weight associated with this playable, don't count it.
			if (!Mathf.Approximately(inputWeight, 0f))
				currentInputs++;
		}

		// If any blending playable exists
		if (currentInputs > 0)
		{
			// Set blended values in the scene reference (track binding)
			for (int i = 0; i < trackBinding.effectStack.Count; i++)
			{
				// Int
				for (int intIndex = 0; intIndex < trackBinding.effectStack[i].ints.Count; intIndex++)
					if (trackBinding.effectStack[i].ints[intIndex].name == blendedInt.name)
						trackBinding.effectStack[i].ints[intIndex].value = blendedInt.value;

				// Float
				for (int floatIndex = 0; floatIndex < trackBinding.effectStack[i].floats.Count; floatIndex++)
					if (trackBinding.effectStack[i].floats[floatIndex].name == blendedFloat.name)
						trackBinding.effectStack[i].floats[floatIndex].value = blendedFloat.value;

				// Color
				for (int colorIndex = 0; colorIndex < trackBinding.effectStack[i].colors.Count; colorIndex++)
					if (trackBinding.effectStack[i].colors[colorIndex].name == blendedColor.name)
						trackBinding.effectStack[i].colors[colorIndex].value = blendedColor.value;
			}
		}
	}

	public override void OnPlayableDestroy(Playable playable)
	{
		firstFrameHappened = false;

		if (trackBinding == null)
			return;
		/*
		// Copy modifiable values
		for (int i=0; i<trackBinding.effectStack.Count; i++)
		{
			for (int intIndex = 0; intIndex<effectStack[i].ints.Count; intIndex++)
			{
				trackBinding.effectStack[i].ints[intIndex].value = effectStack[i].ints[intIndex].value;
			}
			for (int floatIndex = 0; floatIndex < effectStack[i].ints.Count; floatIndex++)
			{
				trackBinding.effectStack[i].floats[floatIndex].value = effectStack[i].floats[floatIndex].value;
			}
			for (int colorIndex = 0; colorIndex < effectStack[i].ints.Count; colorIndex++)
			{
				trackBinding.effectStack[i].colors[colorIndex].value = effectStack[i].colors[colorIndex].value;
			}
		}
		*/
	}

}
