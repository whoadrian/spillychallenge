﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

// Timeline clip of the Effects Stack script.

[Serializable]
public class EffectsStackClip : PlayableAsset, ITimelineClipAsset
{
	public EffectsStackBehaviour template = new EffectsStackBehaviour();

	// Allow blending
	public ClipCaps clipCaps
	{
		get { return ClipCaps.Blending; }
	}

	// Create playable clip
	public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
	{
		var playable = ScriptPlayable<EffectsStackBehaviour>.Create(graph, template);
		return playable;
	}
}
