﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

// Timeline behaviour for the EffectsStack script. Holds relevant timeline data.

[Serializable]
public class EffectsStackBehaviour : PlayableBehaviour
{
	public ShaderInt intData; // Change an int of the Effects Stack materials list
	public ShaderFloat floatData; // Change a float of the Effects Stack materials list
	public ShaderColor colorData; // Change a color of the Effects Stack materials list
}
