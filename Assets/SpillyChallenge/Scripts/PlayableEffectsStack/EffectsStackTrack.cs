﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Collections.Generic;

// Timeline track for the Effects Stack script.

[TrackColor(0.875f, 0.5944853f, 0.1737132f)]
[TrackClipType(typeof(EffectsStackClip))]
[TrackBindingType(typeof(EffectsStack))]
public class EffectsStackTrack : TrackAsset
{
	public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
	{
		return ScriptPlayable<EffectsStackMixer>.Create(graph, inputCount);
	}

	public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
	{
		/*
#if UNITY_EDITOR
		EffectsStack trackBinding = director.GetGenericBinding(this) as EffectsStack;
		if (trackBinding == null)
			return;

		var serializedObject = new UnityEditor.SerializedObject(trackBinding);
		var iterator = serializedObject.GetIterator();
		while (iterator.NextVisible(true))
		{
			if (iterator.hasVisibleChildren)
				continue;

			driver.AddFromName<EffectsStack>(trackBinding.gameObject, iterator.propertyPath);
		}
#endif
*/
		base.GatherProperties(director, driver);
	}
}
