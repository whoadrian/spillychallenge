﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

// Video handler

public class VideoController : MonoBehaviour {
	
	[Range(-2, 2)]
	public float playbackSpeed = 1; // Speed of video playback

	[Header("Players")]
	public List<VideoPlayer> videoPlayers; // All video players

	// Helpers
	private float _tempPlaybackSpeed = 0;
	private bool _ready = false;


	private void Update()
	{
		if (videoPlayers.Count > 0)
		{
			// Check if desired playback speed has changed, apply if so.
			if (_tempPlaybackSpeed != playbackSpeed)
			{
				_tempPlaybackSpeed = playbackSpeed;

				foreach(VideoPlayer v in videoPlayers)
				{
					v.playbackSpeed = playbackSpeed;
				}
			}
		}
	}
	
	// Play all videos
	public void Play ()
	{
		foreach (VideoPlayer v in videoPlayers)
			v.Play();
	}

	// Pause all videos
	public void Pause()
	{
		foreach (VideoPlayer v in videoPlayers)
			v.Pause();
	} 

	// Prepare videos, notify when done
	public void Prepare(Action readyCallback)
	{
		StartCoroutine(PrepareRoutine(readyCallback));
	}
	
	// Prepare coroutine
	private IEnumerator PrepareRoutine (Action readyCallback)
	{
		// Prepare
		foreach (VideoPlayer v in videoPlayers)
			v.Prepare();

		// Wait until ready
		bool ready = false;
		while (!ready)
		{
			ready = true;
			foreach(VideoPlayer v in videoPlayers)
			{
				if (!v.isPrepared)
				{
					ready = false;
					yield return null;
					break;
				}
			}
		}
		_ready = true;

		// Notify listeners (if any)
		if (readyCallback != null)
			readyCallback();
	}
}
