﻿
// Samples neighbouring pixels withing a _Radius and checks the alpha. If below threshold, it means that we're on the edge and draws and outputs a _Color.

Shader "Spilly/OutlineDetection"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
		_Radius ("Radius", Float) = 1
		_Threshold ("Alpha Threshold", Float) = 0.5
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			fixed _Radius;
			fixed _Threshold;
			fixed4 _Color;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 result = 0;
				
				// Samples neighbouring pixels withing a _Radius and checks the alpha. If below threshold, it means that we're on the edge and draws and outputs a _Color.

				if (col.a > _Threshold)
				{
					fixed4 pixelUp = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y * _Radius));
					fixed4 pixelDown = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y * _Radius));
					fixed4 pixelRight = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x * _Radius, 0));
					fixed4 pixelLeft = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x * _Radius, 0));

					if (pixelUp.a * pixelDown.a * pixelRight.a * pixelLeft.a  < _Threshold)
					{
						result = _Color;
					}
				}

				return result;
			}
			ENDCG
		}
	}
}
