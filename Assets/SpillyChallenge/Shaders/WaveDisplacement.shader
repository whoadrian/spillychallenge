﻿
// Distortion shader. Creates horizontal waves.

Shader "Spilly/Wave Displacement"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Amount ("Amount", Float) = 1
		_Freq ("Frequency", Float) = 1
		_Speed ("Speed", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed _Freq;
			fixed _Speed;
			fixed _Amount;

			fixed4 frag (v2f i) : SV_Target
			{
				// Creates horizontal moving lines using a sine value
				fixed2 waveLines = lerp(fixed2(1, 0), fixed2(0, 1), (sin(i.uv.y * _Freq * 3.14 + _Time.y * _Speed) + 1) / 2);
				fixed2 waveDisplacement = fixed2(waveLines.x * _Amount - waveLines.y * _Amount, 0);
				fixed4 col = tex2D(_MainTex, i.uv + waveDisplacement);

				return col;
			}
			ENDCG
		}
	}
}
