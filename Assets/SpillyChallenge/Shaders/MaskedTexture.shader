﻿
// Applies a mask to a texture.

Shader "Spilly/MaskedTexture"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Mask ("Mask", 2D) = "white" {}

		// What color to pick out of the mask
		_MaskColor("Mask Color", Color) = (0, 0, 0, 0)
		// Safety range around the _MaskColor, so we don't have to be super precise in choosing the color!
		_ColorRange("Mask Color Range", Float) = 0.1
		_OutputTint("Output Tint", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _Mask;
			fixed4 _MaskColor;
			fixed _ColorRange;
			fixed4 _OutputTint;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 mainCol = tex2D(_MainTex, i.uv);
				fixed4 maskCol = tex2D(_Mask, i.uv);

				// Using step function to avoid if statements
				fixed4 value = step(maskCol, _MaskColor + _ColorRange) * step(_MaskColor - _ColorRange, maskCol);

				mainCol.rgb = mainCol.rgb * _OutputTint.rgb;

				return mainCol * value.r * value.g * value.b * value.a;
			}
			ENDCG
		}
	}
}
