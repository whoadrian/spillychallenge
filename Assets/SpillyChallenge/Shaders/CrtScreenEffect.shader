﻿
// Creates horizontal moving lines using a sine value applied to the y component of the UVs. Applies only onto non-transparent pixels.

Shader "Spilly/CrtEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
		_Freq ("Frequency", Float) = 1
		_Speed ("Speed", Float) = 1
		_Amount ("Amount", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 _Color;
			fixed _Freq;
			fixed _Speed;
			fixed _Amount;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 effect = 0;
				if (col.a > 0)
				{
					// Creates horizontal moving lines using a sine value applied to the y component of the UVs.
					effect.rgb = col.rgb + sin(i.uv.y * _Freq * 3.14 + _Time.y * _Speed) * _Color; 
					effect.a = _Color.a;
				}
				return lerp(col, saturate(effect), _Amount);
			}
			ENDCG
		}
	}
}
